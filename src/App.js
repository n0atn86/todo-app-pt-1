import React, { Component } from "react";
import todosList from "./todos.json";
// eslint-disable-next-line
import { v4 as uuidv4, v4 } from "uuid";

class App extends Component {
	state = {
		todos: todosList,
		newItem: "",
	};

	addTodo = (todo) => {
		this.setState({ todos: [...this.state.todos, todo] });
	};

	handleSubmit = (event) => {
		event.preventDefault();
		const newTodo = {
			userId: 1,
			id: v4(),
			title: this.state.newItem,
			completed: false,
		};
		this.addTodo(newTodo);
		this.setState({ newItem: "" });
	};

	clearCompleted = () => {
		const { todos } = this.state;
		const newTodoList = todos.filter((todos) => !todos.completed);
		this.setState({ todos: newTodoList });
	};

	handleChange = (event) => {
		this.setState({ newItem: event.target.value });
	};

	toggleCompleted = (checkId) => {
		const newTodo = this.state.todos.map((TodoItem) => {
			if (TodoItem.id === checkId) {
				TodoItem.completed = !TodoItem.completed;
			}
			return TodoItem;
		});
		this.setState({ todos: newTodo });
	};
	destroyItem = (checkId) => {
		const newTodo = this.state.todos.filter(
			(TodoItem) => TodoItem.id !== checkId
		);
		this.setState({ todos: newTodo });
	};

	render() {
		return (
			<section className="todoapp">
				<header className="header">
					<h1>todos</h1>
					<form onSubmit={this.handleSubmit}>
						<input
							onChange={this.handleChange}
							value={this.state.newItem}
							className="new-todo"
							placeholder="What needs to be done?"
							autoFocus
						/>
					</form>
				</header>
				<TodoList
					todos={this.state.todos}
					completed={this.state.todos.completed}
					toggleCompleted={this.toggleCompleted}
					destroyItem={this.destroyItem}
				/>
				<footer className="footer">
					<span className="todo-count">
						<strong>{"0"}</strong> item(s) left
					</span>
					<button
						onClick={() => this.clearCompleted()}
						className="clear-completed"
					>
						Clear completed
					</button>
				</footer>
			</section>
		);
	}
}

class TodoItem extends Component {
	render() {
		return (
			<li className={this.props.completed ? "completed" : ""}>
				<div className="view">
					<input
						className="toggle"
						type="checkbox"
						onChange={() => this.props.toggleCompleted(this.props.id)}
						checked={this.props.completed}
					/>
					<label>{this.props.title}</label>
					<button
						onClick={() => this.props.destroyItem(this.props.id)}
						className="destroy"
					/>
				</div>
			</li>
		);
	}
}

class TodoList extends Component {
	render() {
		return (
			<section className="main">
				<ul className="todo-list">
					{this.props.todos.map((todo) => (
						<TodoItem
							title={todo.title}
							completed={todo.completed}
							id={todo.id}
							toggleCompleted={this.props.toggleCompleted}
							destroyItem={this.props.destroyItem}
						/>
					))}
				</ul>
			</section>
		);
	}
}

export default App;
